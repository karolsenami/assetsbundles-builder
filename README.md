Unity 2019.2.6f1


En gros pour lancer le build il faut que les dossiers à compresser aient un tag AssetBundle (en bas dans l'inspecteur).
N'oublie pas d'enlever les fichiers inutiles (readme, thumbs.db, baseImage...)


#Lancer build:
Build > Build For iOS


#Add tag :
En bas à droite de l'inspecteur une fois le dossier sélectionné.

![add tag](https://gitlab.com/karolsenami/assetsbundles-builder/raw/master/Capture_d_e%CC%81cran_2019-12-02_a%CC%80_18.20.54.png)

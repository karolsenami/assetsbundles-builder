﻿using UnityEngine;
using UnityEditor;


public class BuildiOSAssetBundles : MonoBehaviour
{
    [InitializeOnLoadMethod]
    static void SetupResourcesBuild()
    {
        UnityEditor.iOS.BuildPipeline.collectResources += CollectResources;
    }

    static UnityEditor.iOS.Resource[] CollectResources()
    {
        return new UnityEditor.iOS.Resource[]
        {
            new UnityEditor.iOS.Resource( "game4", "Assets/ODR/game4" ).AddOnDemandResourceTags( "game4" ),
            new UnityEditor.iOS.Resource( "game3", "Assets/ODR/game3" ).AddOnDemandResourceTags( "game3" ),
            new UnityEditor.iOS.Resource( "p010", "Assets/ODR/p010" ).AddOnDemandResourceTags( "p010" ),
            new UnityEditor.iOS.Resource( "p020", "Assets/ODR/p020" ).AddOnDemandResourceTags( "p020" ),
            new UnityEditor.iOS.Resource( "p030", "Assets/ODR/p030" ).AddOnDemandResourceTags( "p030" )
        };
    }

    [MenuItem("Bundle/Build iOS AssetBundle")]
    static void BuildAssetBundles()
    {
        var options = BuildAssetBundleOptions.ChunkBasedCompression;

        bool shouldCheckODR = EditorUserBuildSettings.activeBuildTarget == BuildTarget.iOS;
        /*
#if UNITY_TVOS
            shouldCheckODR |= EditorUserBuildSettings.activeBuildTarget == BuildTarget.tvOS;
#endif

        if (shouldCheckODR)
        {
#if ENABLE_IOS_ON_DEMAND_RESOURCES
            if( PlayerSettings.iOS.useOnDemandResources )
                options |= BuildAssetBundleOptions.UncompressedAssetBundle;
#endif

#if ENABLE_IOS_APP_SLICING
            options |= BuildAssetBundleOptions.UncompressedAssetBundle;
#endif

        }
        */
        BuildPipeline.BuildAssetBundles("Assets/ODR", options, EditorUserBuildSettings.activeBuildTarget);
    }

}
﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.iOS;
using System;
using System.Collections;
using System.IO;

public class LoadBundle : MonoBehaviour
{
    [HideInInspector]
	public AssetBundle Bundle;
	public string bundleTag;

	void Awake()
	{
        Load();
	}

    public void Load()
    {
        Bundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, bundleTag));
        if (Bundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            return;
        }
        //else
        //    LogBundle(Bundle);
    }



	public IEnumerator LoadAsset(string resourceName, string odrTag)
	{
		// Create the request
		OnDemandResourcesRequest request = OnDemandResources.PreloadAsync(new string[] { odrTag });

		// Wait until request is completed
		yield return request;

		// Check for errors
		if (request.error != null)
			throw new Exception("ODR request failed: " + request.error);

		Bundle = AssetBundle.LoadFromFile("res://" + resourceName);
        LogBundle(Bundle);
		request.Dispose();
	}

    public void UnloadBundle()
	{

	}

    void LogBundle(AssetBundle bundle)
    {
        if(bundle == null)
        {
            Debug.Log("null"); return;
        }
        string[] assetsNames = bundle.GetAllAssetNames();
        for (int i = 0; i < assetsNames.Length; i++) Debug.Log(assetsNames[i]);
    }
}